#include<iostream>
#include"fraction.h"
#include<Windows.h>
using namespace std;

int main()
{
	fraction f1;
	cout << "Enter Numerator for fraction1 : ";	//input numerator1.
	f1.getnum();
	cout << "Enter Denominator for fraction1 : ";	//input denominator1.
	f1.getdenom();
	cout << endl;
	fraction f2;
	cout << "Enter Numerator for fraction2 : ";	//input numerator2.
	f2.getnum();
	cout << "Enter Denominator for fraction2 : ";	//input denominator2.
	f2.getdenom();

	f1 + f2;
	cout << "f1 + f2 = ";
	f1.output();
	cout << endl << "f1++ = ";
	++f1; //f1++ doesn't work for some reason
	f1.output();
	cout << endl << "f1 - f2 = ";
	f1 - f2;
	f1.output();
	cout << endl << "f1-- = ";
	--f1; //f1-- doesn't work for some reason
	f1.output();
	cout << endl << "f1*2 = ";
	f1 * 2;
	f1.output();
	cout << endl << "f1/2 = ";
	f1 / 2;
	f1.output();
	cout << endl << "f1*f2 = ";
	f1 * f2;
	f1.output();
	cout << endl << "f1/f2 = ";
	f1 / f2;
	f1.output();
	cout << endl;

	if (f1 == f2)
	{
		f1.output();
		cout << " is equal to ";
		f2.output();
		cout << endl;
	}
	if (f1 >= f2)
	{
		f1.output();
		cout << " is more than or equal to ";
		f2.output();
		cout << endl;
	}
	if (f1 <= f2)
	{
		f1.output();
		cout << " is less than or equal to ";
		f2.output();
		cout << endl;
	}
	if (f1 != f2)
	{
		f1.output();
		cout << " is not equal to ";
		f2.output();
		cout << endl;
	}
	if (f1 < f2)
	{
		f1.output();
		cout << " is less than ";
		f2.output();
		cout << endl;
	}
	if (f1 > f2)
	{
		f1.output();
		cout << " is more than ";
		f2.output();
		cout << endl;
	}

	system("PAUSE");
	return 0;
}