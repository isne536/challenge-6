#include"fraction.h"
#include<iostream>

using namespace std;

fraction::fraction()
{
	num = 1;
	denom = 1;
}

fraction::fraction(int newnum, int newdenom)
{
	num = newnum;
	denom = newdenom;
}

void fraction::setnum(int newnum)
{
	num = newnum;
}

void fraction::setdenom(int newdenom)
{
	denom = newdenom;
}

void fraction::output()
{
	cout << num << "/" << denom;
}

int fraction::getnum()
{
	cin >> num;
	return num;
}

int fraction::getdenom()
{
	cin >> denom;
	return denom;
}
//down here is overload of operations


void operator +(fraction &f1, fraction f2)	//function sum fraction.
{
	int x = (f1.num*f2.denom) + (f2.num*f1.denom);
	int y = f1.denom*f2.denom;

	cout << x << "/" << y << endl;
}

void operator +(fraction &f, int i)
{
	f.num += (f.denom*i);
}

void operator ++(fraction &f)	//function plus one.
{
	f.num += f.denom;
}

void operator -(fraction &f1, fraction f2)	//function minus fraction.
{
	f1.num *= f2.denom;
	f1.num -= (f1.denom*f2.num);
	f1.denom *= f2.denom;
}

void operator -(fraction &f, int i)
{
	f.num -= (f.denom*i);
}

void operator --(fraction &f)	//function minus one.
{
	f.num -= f.denom;
}

void operator *(fraction &f1, fraction f2)	//function product fraction.
{
	f1.num *= f2.num;
	f1.denom *= f2.denom;
}

void operator *(fraction &f, int i)
{
	f.num *= i;
}

void operator /(fraction &f1, fraction f2)	//function divide fraction.
{
	f1.num *= f2.denom;
	f1.denom *= f2.num;
}

void operator /(fraction &f, int i)
{
	if (f.num % i == 0)
	{
		f.num /= i;
	}
	else
	{
		f.denom *= i;
	}
}

bool operator ==(fraction f1, fraction f2)	//function check equal between 2 fractions.
{
	if ((f1.num*f2.denom) == (f2.num*f1.denom))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool operator <=(fraction f1, fraction f2)	//functon less than or equal.
{
	if ((f1.num*f2.denom) <= (f2.num*f1.denom))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool operator >=(fraction f1, fraction f2)	//functon more than or equal.
{
	if ((f1.num*f2.denom) >= (f2.num*f1.denom))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool operator !=(fraction f1, fraction f2)	//functon not equal.
{
	if ((f1.num*f2.denom) != (f2.num*f1.denom))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool operator <(fraction f1, fraction f2)	//functon less than.
{
	if ((f1.num*f2.denom) < (f2.num*f1.denom))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool operator >(fraction f1, fraction f2)	//functon more than.
{
	if ((f1.num*f2.denom) > (f2.num*f1.denom))
	{
		return true;
	}
	else
	{
		return false;
	}
}