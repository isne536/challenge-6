#ifndef FRACTION_H
#define FRACTION_H

class fraction
{
public:
	fraction();
	fraction(int newnum, int newdenom);
	void setnum(int newnum);
	void setdenom(int newdenom);
	int getnum();
	int getdenom();
	void output();

	friend void operator +(fraction &f1, fraction f2);	//function sum fraction.
	friend void operator +(fraction &f, int i);
	friend void operator ++(fraction &f);				//function plus one.
	friend void operator -(fraction &f1, fraction f2);	//function minus fraction.
	friend void operator -(fraction &f, int i);
	friend void operator --(fraction &f);				//function minus one.
	friend void operator *(fraction &f1, fraction f2);	//function product fraction.
	friend void operator *(fraction &f, int i);
	friend void operator /(fraction &f1, fraction f2);	//function divide fraction.
	friend void operator /(fraction &f, int i);
	friend bool operator ==(fraction f1, fraction f2);	//function check equal between 2fractions.
	friend bool operator <=(fraction f1, fraction f2);	//functon less than or equal.
	friend bool operator >=(fraction f1, fraction f2);	//functon more than or equal.
	friend bool operator !=(fraction f1, fraction f2);	//functon not equal.
	friend bool operator <(fraction f1, fraction f2);	//functon less than.
	friend bool operator >(fraction f1, fraction f2);	//functon more than.

private:
	int num;
	int denom;
};

#endif// !FRACTION_H#pragma once
#pragma once
